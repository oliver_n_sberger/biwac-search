<?php


class FileSearchExtension extends DataExtension {
	
	private static $db = array(
		'MetaKeywords' => 'Text',
	);
	
	
	/**
	 * Security tab for folders
	 */
	public function updateCMSFields(FieldList $fields) {
		
		$enabled = Config::inst()->get('SearchConfiguration', 'searchInFiles');
		
		if($enabled == true){
			$owner = $this->owner;
			
			if($owner->ClassName != 'Folder'){
				$fields->push(TextareaField::create('Content', 'Beschreibung'));
				$fields->push(TextField::create('MetaKeywords', 'MetaKeywords'));
				$fields->push(CheckboxField::create('ShowInSearch', 'In Suche anzeigen'));
			}
		}
		
	}
	
}


<?php
/**
 * Suchfeld mit Ausblendung des Inhaltes bei "onblur" und "onfocus"
 * Search box with suppression of the contents of "onblur" and "onfocus"
 * @author Christian Huebschi [christian.huebschi@biwac.ch]
 * @package forms
 * @subpackage fields-basic
 */
class CustomSearchField extends FormField {
	protected $maxLength;
	
	/**
	 * Returns an input field, class="text" and type="text" with an optional maxlength
	 */
	function __construct($name, $title = null, $value = "", $maxLength = null, $form = null){
		$this->maxLength = $maxLength;
		parent::__construct($name, $title, $value, $form);
	}

	function getAttributes() {
		
        $value = $this->Value();
        $jsTitel = _t('CustomSearchField.SEARCH', 'Suchen...');
        
        if(empty($value)){
            $value = $jsTitel;
        }
		
		return array_merge(
			parent::getAttributes(),
			array(
                'onfocus' => 'if(this.value == \'' . $jsTitel . '\')this.value=\'\';',
                'onblur' => 'if(this.value==\'\')this.value=\'' . $jsTitel . '\';',
                'type' => 'text',
                'class' => 'text' . ($this->extraClass() ? $this->extraClass() : ''),
                'id' => $this->id(),
                'name' => $this->getName(),
                'value' => $value,
                'maxlength' => ($this->maxLength) ? $this->maxLength : null,
                'size' => ($this->maxLength) ? min($this->maxLength, 30) : null
			)
		);
	}
	
}
?>
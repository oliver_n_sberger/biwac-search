<?php

/**
 * Custom Search form
 *
 * Durchsucht alle in der Config definierten Klasen. Wenn die Übersetzung aktiv sind
 * oder das Subsite Modul installiert ist, werden nur die Resultat ausgegeben für die
 * aktuelle Sprache und Subsite.
 * 
 * Durchsucht auch DataObject und gibt diese auch aus. Unterstützt bis jetzt nur has_one beziehungen.
 *
 * @author Oliver Nösberger <oliver.noesberger@biwac.ch>
 *  
 */
class CustomSearchForm extends Form {

    /**
     * @var int $pageLength How many results are shown per page.
     * Relies on pagination being implemented in the search results template.
     */
    protected $pageLength = 10;

    /**
     * 
     * @param Controller $controller
     * @param string $name The name of the form (used in URL addressing)
     * @param FieldSet $fields Optional, defaults to a single field named "Search". Search logic needs to be customized
     *  if fields are added to the form.
     * @param FieldSet $actions Optional, defaults to a single field named "Go".
     */
    function __construct($controller, $name, $fields = null, $actions = null) {
        if (!$fields) {
            $fields = new FieldSet(
            	new TextField('Search', _t('CustomSearchForm.SEARCH', 'Suchen...'))
            );
        }

        if (!$actions) {
            $actions = new FieldSet(
            	new FormAction("getResults", _t('CustomSearchForm.GO', 'Go'))
            );
        }

        parent::__construct($controller, $name, $fields, $actions);

        $this->setFormMethod('get');

        $this->disableSecurityToken();
    }

    public function forTemplate() {
        return $this->renderWith(array(
            'CustomSearchForm',
            'Form'
        ));
    }

    /**
     * Get the classes to search
     *
     * @return array
     */
    function getClassesToSearch() {
        return CustomSearch::$searchableClasses;
    }

    /**
     * Return dataObjectSet of the results using $_REQUEST to get info from form.
     * Wraps around {@link searchEngine()}.
     *
     * @param int $pageLength
     * @param array $data Request data as an associative array. Should contain at least a key 'Search' with all searched keywords.
     * @return DataObjectSet
     */
    public function getCustomResults($pageLength = null, $data = null) {
        // legacy usage: $data was defaulting to $_REQUEST, parameter not passed in doc.silverstripe.org tutorials
        if (!isset($data) || !is_array($data))
            $data = $_REQUEST;

        $keywords = $data['Search'];

        if (!$pageLength)
            $pageLength = $this->pageLength;
        $start = isset($_GET['start']) ? (int) $_GET['start'] : 0;

        $results = CustomSearch::getObjectsSearchResults($keywords, $start, $pageLength);

        return $results;
    }
    
    /**
     * Get the search query for display in a "You searched for ..." sentence.
     * 
     * @param array $data
     * @return string
     */
    public function getSearchQuery($data = null) {
        // legacy usage: $data was defaulting to $_REQUEST, parameter not passed in doc.silverstripe.org tutorials
        if (!isset($data))
            $data = $_REQUEST;

        return Convert::raw2xml($data['Search']);
    }    

    /**
     * Set the maximum number of records shown on each page.
     *
     * @param int $length
     */
    public function setPageLength($length) {
        $this->pageLength = $length;
    }

    /**
     * @return int
     */
    public function getPageLength() {
        return $this->pageLength;
    }

}

?>

<?php
/***
 * 
 * @author Oliver Noesberger <oliver.noesberger@biwac.ch>
 * 
 * In Classe einfügen für MyISAM Tabelle
 * static $create_table_options = array(
    'MySQLDatabase' => 'ENGINE=MyISAM'
	);
	
	Dublikate verhindern mit einem GROUP BY <col> und im Select COUNT(<col>)
	
	Volltextsuche mit Match(): http://dev.mysql.com/doc/refman/5.1/de/fulltext-search.html
 * 
 */
class CustomSearch extends Object {

    static $searchableClasses = array();
    static $extraDataObjects = array();
    static $searchablePageClasses = array();
    static $parentClasses = array();
    static $autocomplet = array();


    
    /**
     * Gibt Klassen fuer Suche aus
     * @author Oliver Noesberger <oliver.noesberger@biwac.ch>
     * @param string $typ 
     * @return array()
     */
    public static function ClassesForSearch($typ){
        if($typ == 'PageClasses'){
            $search_text = Config::inst()->get('SearchConfiguration', 'searchablePageClasses');
        }elseif($typ == 'ObjectClasses'){
            $search_text = Config::inst()->get('SearchConfiguration', 'searchableDataObjectClasses');
        }elseif($typ == 'SpecialObjectClasses'){
            $search_text = Config::inst()->get('SearchConfiguration', 'extraDataObjects');
        }
        
        $explod_text = explode(',', $search_text);
        
        $array = array();
        foreach ($explod_text as $key => $value) {
            $array[] = trim($value);
        }
        
        return $array;
    }
    
    /**
     * Gibt Klassen und Felder, die im Multiautocomplet durchsucht werden sollen, aus
     * @author Oliver Noesberger <oliver.noesberger@biwac.ch>
     * @return array()
     */
    public static function ParentsForSearchClasses(){
        $search_text = Config::inst()->get('SearchConfiguration', 'parentsForSearchableDataObject');
        
        $explod_text = explode('],', $search_text);

        $array = array();
        foreach ($explod_text as $key => $value) {
            $classnames = explode('[', $value);
            
            $count = 0;
            $count++;
            $pos = strpos($classnames[1], ',');

            if ($pos !== false) {
                $parent = array();
                $explod_text3 = explode(',', $classnames[1]);
                foreach ($explod_text3 as $key3 => $value3) {
                    $name = str_replace(']', '', $value3);
                    $parent[] = trim($name);
                }
                $array[trim($classnames[0])] = $parent;
            }else{
                $array[trim($classnames[0])] = str_replace(']', '', $classnames[1]);
            }
        }
        
        return $array;
    }

    
    /**
     * Speichert alle zu durchsuchende Page Klassen, die in der Config angegeben worden sind,
     * in einer Variablen
     * 
     * @author Oliver Noesberger <oliver.noesberger@biwac.ch>
     */
    public static function searchablePageClasses() {
		
		$classNames = self::ClassesForSearch('PageClasses');
		
        foreach ($classNames as $className){
        	if(!empty($className)){
	            self::$searchableClasses[] = $className;
	            self::$searchablePageClasses[] = $className;
        	}
        }
    }
    
    /**
     * Speichert alle zu durchsuchende DataObject Klassen, die in der Config angegeben worden sind,
     * in einer Variablen
     * 
     * @author Oliver Noesberger <oliver.noesberger@biwac.ch>
     * @param array $classNames 
     */
    public static function searchableDataObjectClasses() {
		
		$classNames = self::ClassesForSearch('ObjectClasses');

        foreach ($classNames as $className)
            self::$searchableClasses[] = $className;
    }    
    
    /**
     * Speichert speziellen DataObject Klassen, die in der Config angegeben worden sind, 
     * in einer Variablen
     * 
     * @author Oliver Noesberger <oliver.noesberger@biwac.ch>
     * @param array $classNames 
     */
    public static function extraDataObjects() {
		
		$DataObjects = self::ClassesForSearch('SpecialObjectClasses');

        foreach ($DataObjects as $DataObject)
            self::$extraDataObjects[] = $DataObject;
    }
	
    /**
     * Speichert alle Parents fuer die jeweiligen DataObjects
     * 
     * @author Oliver Noesberger <oliver.noesberger@biwac.ch>
     * @param array $classNames 
     */
	public static function ParentForDataObject() {
		
		$DataObjects = self::ParentsForSearchClasses();

		foreach ($DataObjects as $key => $value) {
			self::$parentClasses[$key] = $value;
		}
	}

    /**
     * Druchsucht alle DataObjects die in der Config bestimmt werden
     *
     * @author Oliver Noesberger <oliver.noesberger@biwac.ch>
     * @global type $searchableDataObjects
     * @param type $data
     * @return DataObjectSet
     */
    public static function getObjectsSearchResults($data, $start, $pageLength){//, $request) {
    	
		CustomSearch::searchablePageClasses();
		CustomSearch::searchableDataObjectClasses();
		CustomSearch::extraDataObjects();
		CustomSearch::ParentForDataObject();

        // Holt Suchbegriff und Cleant diesen
        $keywords = Convert::raw2sql($data);
        $searchKeywords = $keywords;
        
        // Holt alle zu Durchsuchenden DataObjects
        $searchableObjects = self::$searchableClasses;
        $searchablePageObjects = self::$searchablePageClasses;
        
        // Wenn keine Klassen in der Config angegeben wurden
        if(empty ($searchableObjects)){
            throw new Exception('Keine zu durchsuchende Klassen configuriert.');
            exit;
        }
        $dataOjects = array();
        if (!empty($searchableObjects)) {

            $totalCount = 0;
            $resultArray = array();
            // Erstellt mit der Vorlage ein SQLQuery und liest dann alle existierende
            // Felder aus(SQLQuery gibt einen verwendbaren Array zurück, DataObject::get
            // nicht) und erstellt mit diesen den SuchQuery
            foreach ($searchableObjects as $key => $objectName) {
                $DataObject = new $objectName();
				//@todo Problem mit durchsuchen bei PageObjects
                if(in_array($objectName, $searchablePageObjects)){
					$dataObject_fields = '';
				}else{
					$dataObject_fields = $DataObject->db();	
				}
                // Erstellt Where
				if(!empty($dataObject_fields)){
	                $where = self::getWhere($searchKeywords, $dataObject_fields, $objectName);
			                
	                // Live und nicht Live problematik
	                //@todo Lösung finden für DataObject und bessere für SiteTree
	                if(in_array($objectName, $searchablePageObjects) ||
                       singleton($objectName)->hasExtension('Versioned') ||
	                   $objectName == 'SiteTree'){
	                    $searchQuery = new SQLQuery('*', $objectName.'_Live', $where);
	                }else{
	                    $searchQuery = new SQLQuery('*', $objectName, $where);
	                }
	
	                $searchResultObject = $searchQuery->execute();
	
	                // Erstellt Result Array zur Weiterverarbeitung
	                foreach ($searchResultObject as $row) {
	                    // Gibt Treffer aus
	                    $treffer = self::countTreffer($row, $searchKeywords, $objectName);
	                    if (in_array($objectName, $searchablePageObjects) ||
	                        $objectName == 'SiteTree') {
	                        if($objectName == 'SiteTree'){
	                            if ($row['ShowInSearch'] == 1 &&
	                                $row['ClassName'] != 'ErrorPage') {
	                                ++$totalCount;
	                                $resultArray[$totalCount]['treffer'] = $treffer;
	                                $resultArray[$totalCount]['data'] = $row;
	                            }
	                        }else{
	                            $row = self::changetoSiteTreeObject($row, $objectName, $keywords);
	                            ++$totalCount;
	                            $resultArray[$totalCount]['treffer'] = $treffer;
	                            $resultArray[$totalCount]['data'] = $row;
	                        }
						} elseif($objectName == 'File'){
							if($row['ShowInSearch'] == 1 &&
                               $row['ClassName'] == 'File'){
								$row = self::addExtraFieldsForFiles($row);
		                        ++$totalCount;
		                        $resultArray[$totalCount]['treffer'] = $treffer;
		                        $resultArray[$totalCount]['data'] = $row;
							}
	                    } else {
	                        $row = self::addExtraFields($row);
	                        ++$totalCount;
	                        $resultArray[$totalCount]['treffer'] = $treffer;
	                        $resultArray[$totalCount]['data'] = $row;
	                    }
	                }
            	}
			}
			/*
			$filewhere = self::getWhere($searchKeywords, array('Name' => 'Text', 'Title' => 'Text'), 'File');
			$filesearchQuery = new SQLQuery('*', 'File', $filewhere);
			$fileResult = $filesearchQuery->execute();
			foreach ($fileResult as $file_row) {
				$file_treffer = self::countTreffer($file_row, $searchKeywords, 'File');
			}
			*/
        }

        // Entfernt Dublicate(DataObjects)
        $noDublicatResultArray = self::dubplicatCheck($resultArray);
         
        // Sort Results
        arsort($noDublicatResultArray);

        // Anzahl der Objects
        $totalCount = self::countAllObject($noDublicatResultArray);
        
        // Pusht alle Resultate in eine ArrayList()
        $ResultObject = self::pushObject($noDublicatResultArray);
        
        // Fühgt Pageing ein
        if($ResultObject) {
	        $results = new PaginatedList($ResultObject);//, $request);
	        $results->setPageStart($start);
	        $results->setPageLength($pageLength);
	        $results->setTotalItems($totalCount);
    	}
        
        return $results;
    }

    /**
     * Erstellt Where abfrage für jedes Object
     *
     * @author Oliver Noesberger <oliver.noesberger@biwac.ch>
     * @param type $keywords
     * @param type $DataObject
     * @return string
     */
    private static function getWhere($keywords, $DataObjectfields, $currentClassName) {

        $searchablePageObjects = self::$searchablePageClasses;
        $where = '';
        $numItems = 0;
        foreach ($DataObjectfields as $field => $typ) {
            if($field != 'ID' &&
               $field != 'ClassName' &&
               $field != 'Created' &&
               $field != 'LastEdited'){
                $pos = strpos($typ, 'Varchar(');
                if($typ == 'Text' || $typ == 'HTMLText' || $pos !== false){
                    ++$numItems;
                }
           }
        }
        $i = 0;
        foreach ($DataObjectfields as $field => $typ) {
            if($field != 'ID' &&
               $field != 'ClassName' &&
               $field != 'Created' &&
               $field != 'LastEdited'){
                $pos = strpos($typ, 'Varchar(');
                if($typ == 'Text' || $typ == 'HTMLText' || $pos !== false){
                    ++$i;
                    // Abfrage ohne keysensitiv
                    $where .= $field . " Like '%" . $keywords . "%' COLLATE utf8_general_ci";
                    if ($i != $numItems) {
                        $where .= ' OR ';
                    }
                }
            }
        }

        return $where;
    }
    
    private static function changetoSiteTreeObject($row, $objectName, $keywords){
        
        $SiteTree = DataObject::get_by_id('SiteTree', $row['ID']);
        
        $row['ClassName'] = $SiteTree->ClassName;
        $row['URLSegment'] = $SiteTree->URLSegment;
        $row['Title'] = $SiteTree->Title;
        $row['ExtraSiteTree'] = true;
        
        $content = '';
        foreach ($row as $key => $value){
            if(preg_match("/".$keywords."/", $value)){
                $content .= $value;
            }
        }
        
        $row['Content'] = $content;
        
        return $row;
    }
	
	private static function addExtraFieldsForFiles($row){
		
		$row['Link'] = $row['Filename'];
		
		return $row;
	}

    /**
     * Füght bei alle DataObjects die kein SiteTree sind die Nötigen ParentFelder
     * aus dem SiteTree hinzu(Link, Sprache, Subsite, Title, Content)
     * 
     * @author Oliver Noesberger <oliver.noesberger@biwac.ch>
     * @todo Bessere zusammenstellung für Späteres auslesen(Damit richtiger Titel etc ausgelesen wird)
     * @param type $row
     * @return type 
     */
    private static function addExtraFields($row) {
		
        $spezialDataObjects = self::$extraDataObjects;
    	
		$locale = false;
		if(singleton('SiteTree')->hasExtension('Translatable')){
			$locale = true;
		}
		
		$subsite = false;
		if(class_exists('Subsite')){
			$subsite = true;
		}

        //Gibt ParentObject zurück
        $parentObject = self::getParent($row, $locale, $subsite);
        
        if(!empty($parentObject)){
            // Prüft ob ParentObject eine spezielle Classe ist
            if(!in_array($row['ClassName'], $spezialDataObjects) &&
               in_array($parentObject->ClassName, $spezialDataObjects)){
                $row['ParentisDataObject'] = $parentObject->ClassName;
            }
        }else{
            // Wenn Parent leer existiert dieser eintrag nicht mehr
            $row['NoExistingParent'] = true;
            return $row;
        }
		
        $dataObjectContent = self::checkforDataObjectContent($row['ClassName'], $row['ID']);
        
        if ($parentObject) {
            $row['ParentPageTitle'] = $dataObjectContent['Title'];
            $row['ParentPageContent'] = $dataObjectContent['Content'];
            if (!in_array($row['ClassName'], $spezialDataObjects)) {
                if(isset($row['ParentisDataObject'])){
                    if (in_array($row['ParentisDataObject'], $spezialDataObjects)) {
                        unset ($row['ParentisDataObject']);
                        $row['ParentPageClassName'] = $parentObject->ClassName;
                        $row['URLSegment'] = $parentObject->Link();
                        $row['Link'] = $parentObject->Link();
                    }
                }else{
                    $row['ParentPageClassName'] = $parentObject->ClassName;
                    $row['URLSegment'] = $parentObject->URLSegment;
                    $row['Link'] = $parentObject->Link();
                }
            }

            if ($locale == true) {
                $row['ParentPageLocale'] = $parentObject->Locale;
            }
            if ($subsite == true) {
                $row['ParentSubsiteID'] = $parentObject->SubsiteID;
            }
        }

        return $row;
    }
    
    /**
     * Gibt Parent zurück
     * 
     * @author Oliver Noesberger <oliver.noesberger@biwac.ch>
     * @param type $row
     * @return Parent DataObject()
     */
    private static function getParent($row, $locale, $subsite) {

        $parentObject_array = self::searchForParent($row);
		$spezialDataObjects = self::$extraDataObjects;
		
		if(!empty($parentObject_array)){
			$parentObject = DataObject::get_by_id($parentObject_array['ParentName'], $parentObject_array['ParentID']);
		}else{
			$parentObject = false;
		}
		
		// @todo Subsite variante einbauen
		if($locale == true && $parentObject != false){
			if(empty($parentObject->Locale)){
				if(in_array($parentObject->ClassName, $spezialDataObjects)){
					$map = $parentObject->toMap();
					$nextparentObject = self::getParent($map, $locale, $subsite);
					$parentObject->Locale = $nextparentObject->Locale;
				}else{
					$map = $parentObject->toMap();
					$parentObject = self::getParent($map, $locale, $subsite);
				}
			}
		}
        
        return $parentObject;
    }
    
    /**
     * Sucht mit den aktuellen Eigenschaftsvariablen der Classe nach Parent
     * Fuer has_one und has_many Verbindungen
     * @todo many_many Verbindungen abfangen
     * 
     * @author Oliver Noesberger <oliver.noesberger@biwac.ch>
     * @param type $row
     * @return type 
     */
    private static function searchForParent($row) {
    	
		$parent = '';
		$parents = self::$parentClasses;

        foreach ($parents as $CurrentClass => $ParentClass) {
            if($row['ClassName'] == $CurrentClass){
                if(is_array($ParentClass)){
                    foreach ($ParentClass as $key => $parentClasses) {
                        if(isset($row[$parentClasses . 'ID'])){
                            if(!empty($row[$parentClasses . 'ID']) &&
                                $row[$parentClasses . 'ID'] != 0){
                                $parent['ParentName'] = $parentClasses;
                                $parent['ParentID'] = $row[$parentClasses . 'ID'];
                            }
                        }
                    }
                }else{
                    if(isset($row[$ParentClass . 'ID'])){
                        $parent['ParentName'] = $ParentClass;
                        $parent['ParentID'] = $row[$ParentClass . 'ID'];
                    }
                }
            }
        }

        return $parent;
    }
    
    /**
     * Sucht für das aktuelle DataObject den Titel und Inhalt
     * Im DataObject selbst kann man entweder die Funktion SearchTitle()/SearchContent() machen
     * oder das Feld einfach auch mit Title/Content bezeichnen wie eine normale Page
     * 
     * @author Oliver Noesberger <oliver.noesberger@biwac.ch>
     * @param type $className
     * @param type $id
     * @return string
     */
    private static function checkforDataObjectContent($className, $id){
        
        $search = array();
        
        $currentObject = DataObject::get_by_id($className, $id);
        $Object = new $className();
        
        if(method_exists($Object, 'SearchTitle') == true){
            $titel = $currentObject->SearchTitle();
        }
        
        if(empty($titel)){
            if(preg_match("/#/", $currentObject->Title)){
                $titel = '';
            }else{
                $titel = $currentObject->Title;
            }
        }
        
        if(method_exists($Object, 'SearchContent') == true){
            $content = $currentObject->SearchContent();
        }
        
        if(empty($content)){
            if(empty($currentObject->Content)){
                $content = '';
            }else{
                $content = $currentObject->Content;
            }
        }
        
        if(!empty($content)){
            $content = self::LimitDataObjectContent($content);
        }
        
        $search['Title'] = $titel;
        $search['Content'] = $content;
        
        return $search;
    }
    
    private static function LimitDataObjectContent($content) {
        $numWords = 26;
        $add = '...';
        $content = trim(Convert::xml2raw($content));
        $ret = explode(' ', $content, $numWords + 1);

        if (count($ret) <= $numWords - 1) {
            $ret = $content;
        } else {
            array_pop($ret);
            $ret = implode(' ', $ret) . $add;
        }

        return $ret;
    }

    /**
     * Zählt übereinstimmungen mit dem Suchbegriff
     * 
     * @author Oliver Noesberger <oliver.noesberger@biwac.ch>
     * @param type $row
     * @param type $searchKeywords
     * @param type $objectName
     * @return int 
     */
    private static function countTreffer($row, $searchKeywords, $objectName) {
        
        $treffer = 0;
        foreach ($row as $key => $value) {
            if (preg_match("/$searchKeywords/i", $value)) {
                preg_match_all("/$searchKeywords/i", $value, $trefferarray);
                foreach ($trefferarray as $anzahlarray){
                    foreach ($anzahlarray as $anzahl){
                        ++$treffer;
                    }
                }
            }
        }
        
        return $treffer;
    }   
    
    /**
     * Entfernt Duplicate
     * 
     * @author Oliver Noesberger <oliver.noesberger@biwac.ch>
     * @param type $resultArray
     * @return type 
     */
    private static function dubplicatCheck($resultArray){
        
        // Prüft ob das Subsite Modul installiert wurde
        if (class_exists('Subsite')) {
            //$subsite = Subsite::currentSubsite();
            $currentSubsiteID = Subsite::currentSubsiteID();
            $resultArray = self::checkforSubsite($resultArray, $currentSubsiteID);
        }
        
        // Prüft ob Seite Mehrsprachig ist
        if(singleton('SiteTree')->hasExtension('Translatable')) {
            $currentLocale = Translatable::get_current_locale();
            $resultArray = self::checkforLanguage($resultArray, $currentLocale);
        }
        // NoExistingParent
        $resultArray = self::checkifParentExist($resultArray);
        
        // Liste der Dublicate
        $duplicat_list = array();
        foreach ($resultArray as $key => $dublicateCheck){
            if (!in_array($key, $duplicat_list)) {
                if(isset($dublicateCheck['data']['URLSegment'])){
                    if(isset($dublicateCheck['data']['ParentPageClassName'])){
                        // Gefundenes Object wird aus dem Array entfernt damit
                        // im zweiten foreach nicht nochmals das gleiche gefunden wird
                        unset($resultArray[$key]);
                        
                        $duplicate_found = false;
                        $hit_count = 0;
                        // Sucht nach gleichen Objecte
                        foreach ($resultArray as $isDuplicateCheckKey => $isDuplicateCheckValues){
                            if(isset($isDuplicateCheckValues['data']['URLSegment'])){
                                if ($dublicateCheck['data']['URLSegment'] == $isDuplicateCheckValues['data']['URLSegment']) {
                                    // Falls ein Object gefunden wird Variable auf True gesetzt
                                    $duplicate_found = true;
                                    // Es werden alle Treffer zusammengezält
                                    $hit_count += $isDuplicateCheckValues['treffer'];
                                    // Das gefundene Object wird entfernt und auf die Ignorliste(Dublicat List) gesetzt
                                    unset($resultArray[$isDuplicateCheckKey]);
                                    $duplicat_list[] = $isDuplicateCheckKey;
                                }
                            }
                        }

                        // Zählt Treffer allen gefundenen
                        if ($duplicate_found) {    
                            $dublicateCheck['treffer'] += $hit_count;   
                        }
                        // Gefundenes Object wird auf die Ignorliste(Dublicat List) gesetzt
                        $resultArray[] = $dublicateCheck;

                    }
                }
            }
        }
        
        return $resultArray;
    }
    
    /**
     * Gibt nur Resultat zurück für die Aktuelle Subsite
     * 
     * @author Oliver Noesberger <oliver.noesberger@biwac.ch>
     * @param type $resultArray
     * @param type $currentSubsiteID
     * @return type 
     */
    private static function checkforSubsite($resultArray, $currentSubsiteID) {

        foreach ($resultArray as $key => $checkArray) {
            //if (!empty($currentSubsiteID)) {
                // Überprüfung für SiteTree
                if (isset($checkArray['data']['SubsiteID'])) {
                    if ($currentSubsiteID != $checkArray['data']['SubsiteID']) {
                        unset($resultArray[$key]);
                    }
                }
                // Überprüfung für alle Andere DataObjects
                elseif (isset($checkArray['data']['ParentSubsiteID'])) {
                    if ($currentSubsiteID != $checkArray['data']['ParentSubsiteID']) {
                        unset($resultArray[$key]);
                    }
                }
            //}
        }

        return $resultArray;
    }
    
    /**
     * Gibt nur Resultat zurück für die Aktuelle Sprache
     * 
     * @author Oliver Noesberger <oliver.noesberger@biwac.ch>
     * @param type $resultArray
     * @param type $currentLocale
     * @return type 
     */
    private static function checkforLanguage($resultArray, $currentLocale) {

        foreach ($resultArray as $key => $checkArray) {
            if (!empty($currentLocale)) {
                // Überprüfung für SiteTree
                if (isset($checkArray['data']['Locale'])) {
                    if ($currentLocale != $checkArray['data']['Locale']) {
                        unset($resultArray[$key]);
                    }
                }
                // Überprüfung für alle Andere DataObjects
                elseif (isset($checkArray['data']['ParentPageLocale'])) {
                    if ($currentLocale != $checkArray['data']['ParentPageLocale']) {
                        unset($resultArray[$key]);
                    }
                }
            }
        }

        return $resultArray;
    }
    
    /**
     * Überprüft ob Parent von einem DataObject noch existiert
     * 
     * @author Oliver Noesberger <oliver.noesberger@biwac.ch>
     * @param type $resultArray
     * @return type 
     */
    private static function checkifParentExist($resultArray) {

        foreach ($resultArray as $key => $checkArray) {
            if (isset($checkArray['data']['NoExistingParent'])) {
                if ($checkArray['data']['NoExistingParent'] = 1) {
                    unset($resultArray[$key]);
                }
            }
        }

        return $resultArray;
    }

    
    /**
     * Zählt alle noch vorhandenen Objects(für Pageing)
     * 
     * @param type $allObjects
     * @return int 
     */
    private static function countAllObject($allObjects){
        
        $totalCount = 0;
        foreach ($allObjects as $Object){
            ++$totalCount;
        }
        
        return $totalCount;
    }


    /**
     * Pusht alle Resultat in ein DataObjectSet
     * 
     * @author Oliver Noesberger <oliver.noesberger@biwac.ch>
     * @param type $resultsArray
     * @return DataObjectSet 
     */
    private static function pushObject($resultsArray){
        
        $ResultObject = new ArrayList();
        $currentMember = Member::currentUser();
        
        foreach ($resultsArray as $data) {
            foreach ($data as $key) {
                $ClassName = $key['ClassName'];
            }
            $newObject = new $ClassName($data['data']);

            if ($newObject->can("View", $currentMember)) {
                $ResultObject->push($newObject);
            }
        }
        
        return $ResultObject;
    }
    
}
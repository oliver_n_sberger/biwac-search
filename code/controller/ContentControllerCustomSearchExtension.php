<?php

/**
 * Extension to provide a search interface when applied to ContentController
 *
 */
class ContentControllerCustomSearchExtension extends Extension {

    static $allowed_actions = array(
        'CustomSearchForm',
        'Customresults',
    );

    /**
     * Site search form
     */
    function CustomSearchForm() {

        $searchText = '';
        if ($this->owner->request) {
            $searchText = $this->owner->request->getVar('Search');
        }
        
        if(empty($searchText)){
            $searchText = _t('CustomSearchForm.SEARCH', 'Search / Keywords');
        }
        
        $SearchField = new AutoCompleteCustomField('Search', $searchText, $searchText);
        
        $fields = new FieldList(
        	$SearchField
        );
        $actions = new FieldList(
        	new FormAction('Customresults', ' ')
        );
        $form = new CustomSearchForm($this->owner, 'CustomSearchForm', $fields, $actions);

        return $form;
    }

    /**
     * Process and render search results.
     *
     * @param array $data The raw request data submitted by user
     * @param SearchForm $form The form instance that was submitted
     * @param SS_HTTPRequest $request Request generated for this action
     */
    function Customresults($data, $form, $request) {

        $data = array(
            'Results' => $form->getCustomResults(null, $data, $request),
            'Query' => $form->getSearchQuery(),
            'Title' => _t('CustomSearchForm.SearchResults', 'Search Results')
        );
        return $this->owner->customise($data)->renderWith(array('Page_customresults', 'Page'));
    }

}